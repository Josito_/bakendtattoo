'use strict';

const COLLECTION = 'users';

const admin_user = {
	username: 'admin',
	password: 'admin',
	email: 'admin@gmail.com',
	admin: true,
};

const normal_user = {
	username: 'normaluser',
	password: 'normaluser',
	email: 'normalusertest@gmail.com',
	admin: false,
};

async function up(db) {
	await db.collection(COLLECTION).insertMany([
		admin_user, normal_user
	]);
}

async function down(db) {
	await db.collection(COLLECTION).deleteOne(admin_user);
	await db.collection(COLLECTION).deleteOne(normal_user);
}

module.exports = {
	up,
	down,
};