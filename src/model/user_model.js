const mongoose = require('mongoose');
const schema = mongoose.Schema;

const userSchema = new schema({
	username: { type: String, required: true },
	password: { type: String, required: true },
	email: { type: String, required: true },
	admin: { type: Boolean, required: true, default: false },
});

module.exports = mongoose.model('User', userSchema);
