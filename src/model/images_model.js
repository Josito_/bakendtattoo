const mongoose = require('mongoose');
const schema = mongoose.Schema;

const imageSchema = new schema({
	title: { type: String, required: true },
	description: { type: String, required: true },
	file: { type: String, required: true },
	imageType: String,
});

module.exports = mongoose.model('imageModel', imageSchema);
