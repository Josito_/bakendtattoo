const express = require('express');
const userController = require('../controller/userController');

const app = express();

app.get('/', function (req, res) {
	res.send('Hola');
});

app.post('/new', userController.createUser);

module.exports = app;
