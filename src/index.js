require('dotenv').config();
const express = require ('express');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');
const swaggerUI = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc'); 
const userRoutes = require('./routes/userRouter');
const app = new express();

mongoose.connect(process.env.MONGO_URL);

const swaggerOptions = {
	definition: {
		openapi: '3.0.0',
		info: {
			title: 'Documentacion de los endpoints',
			version: '1.0'
		},
		servers: [
			{
				url: 'http://localhost:9000'
			}
		]
	},
	apis: [
		'./src/routes/*.js'
	]
};


app.set('port',  process.env.PORT | 9000);

app.use(cors());
app.use(morgan('dev'));
app.use(express.json());
app.use('/swagger', swaggerUI.serve, swaggerUI.setup( swaggerJSDoc(swaggerOptions), { explorer: true } ));
app.use('/users', userRoutes);

app.listen(app.get('port'), () => {
	console.log('Running');
});
