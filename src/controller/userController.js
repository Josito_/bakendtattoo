const User = require('../model/user_model');
const { userSchemaValidator } = require('../helper/schema_valdation');

const createUser = async (req, res) => {
	try {
		const validation = await userSchemaValidator.validate(req.body);
		if(!validation.error){
			const user = new User(validation.value);
			await user.save();
			res.status(201).json(user);
		} else res.status(400).send({message: validation.error.details[0].message});
	} catch (error) {
		res.status(400).send({message: error.message});
	}
};

exports.createUser = createUser;
