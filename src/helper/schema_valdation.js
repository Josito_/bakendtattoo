const Joi = require('joi');

const userSchemaValidator = Joi.object({
	username: Joi.string().min(3).max(20).required(),
	password: Joi.string().min(5).required(),
	email: Joi.string().email().lowercase().required(),
});

module.exports = { userSchemaValidator };