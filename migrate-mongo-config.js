const config = {
  mongodb: {
    url: "mongodb+srv://admin:sUk97Jfk66lnUK7W@nonas-web.fm34s.mongodb.net/nonas-web?retryWrites=true&w=majority",
    databaseName: "nonas-web",
    options: {
      useNewUrlParser: true, 
      useUnifiedTopology: true,
    }
  },
  migrationsDir: "migrations",
  changelogCollectionName: "changelog",
  migrationFileExtension: ".js",
  useFileHash: false
};

module.exports = config;
